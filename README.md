# Microservices - Service Registry
## :triangular_ruler: Architecture
The Service Registry is a simple persistence service which holds metadata about each microservice within the ecosystem. Microservices can register, re-register and deregister themselves so core microservices (Service Mesh, Event Hub) can retrieve a list of all microservices and their metadata.

![Architecture](img/architecture.png)

## :pen: Registration
A microservice can register itself via an HTTP POST to the root endpoint of the Service Registry with the following properties:
```json
{
    "name": "user-geofence-notifications",
    "location": "http://localhost:8000",
    "queue_events": [
        "user:entered_geofence",
        "user:exited_geofence"
    ],
    "topic_events": [
        "geofence:created",
        "geofence:updated",
        "geofence:deleted"
    ]
}
```

Each property is explained below
* ***name*** - the unique name of the microservice, this will be used to route requests to the service through the Service Mesh.
* ***location*** - the url of the root of the microservice.
* ***queue_events*** - an array of event names which the service is subscribing to and requires queuing, this is used by the Event Hub to fanout events to a single queue for each microservice, these events will be delivered once to **one** instance of the microservice.
* ***topic_events*** - an array of event names which the service is subscribing to and requires broadcasting via a queue, this is used by the Event Hub to fanout events to a topic for each microservice, these events will be delivered once to **all** instances of the microservice.

After registration the Service Registry will then give each microservice a few additional properties:
* ***status*** - `active` / `inactive` - initally this is `active`, but will be changed to inactive if the Service Monitor cannot contact the service at the location specified. Inactive microservices will not have http requests routed to them, but events will continue to be put in to their queue.
* ***circuit_breaker*** - `closed` / `half_open` / `open` - intially this is `closed`, if the breaker is `open` then http requests will not be routed to the service for a given number of seconds, the status will then change to `half_open`, where all requests will be routed but the moment one fails with a `5xx` error will switch back to `closed`. Events will still be sent to queues and topics regardless of the status.
* ***timestamp*** - the timestamp of when the service was last registered

## :pencil: Logging
Whilst the complete details of all microservices can be retrieved via an http request, the Service Registry will still send the meta data to CloudWatch to allow for monitoring and alerting.

## :chart_with_upwards_trend: Scale
The Service Registry is stateless and thus can be scaled horizontally to hundreds of instances to handle any influx of load, however this is not expeted to be a heavily used service.