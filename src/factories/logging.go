package factories

import (
	"log"
	"os"

	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/logging"
	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/logging/clients/elasticsearch"
	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/logging/clients/stdout"
)

func NewLoggingClient() logging.LoggingClient {
	lgc := os.Getenv("LOGGING_CLIENT")

	switch lgc {
	case "elasticsearch":
		log.Println("[x] Using ElasticSearch for logging")
		return elasticsearch.New()
	default:
		log.Println("[x] Using stdout for logging")
		return stdout.New()
	}
}
