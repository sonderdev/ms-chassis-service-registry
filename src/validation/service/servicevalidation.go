package serviceValidation

import (
	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/dto"
	validator "gopkg.in/go-playground/validator.v9"
)

func ValdidateNewServiceRequest(nsr dto.NewServiceRequest) bool {
	validate := validator.New()
	err := validate.Struct(nsr)

	if err != nil {
		return false
	}
	return true
}
