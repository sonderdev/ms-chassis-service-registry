package dto

import "time"

type Service struct {
	Name             string    `json:"name"`
	Location         string    `json:"location"`
	QueueEvents      []string  `json:"queue_events"`
	TopicEvents      []string  `json:"topic_events"`
	HealthEndpoint   string    `json:"health_endpoint"`
	Status           string    `json:"status"`
	CircuitBreaker   string    `json:"circuit_breaker"`
	Retries          int       `json:"retries"`
	RegistrationTime time.Time `json:"registered_date"`
}
