package dto

type NewServiceRequest struct {
	Name           string   `validate:"required,min=3,max=32"`
	Location       string   `validate:"required,url"`
	HealthEndpoint string   `validate:"required,uri" json:"health_endpoint"`
	QueueEvents    []string `json:"queue_events"`
	TopicEvents    []string `json:"topic_events"`
}
