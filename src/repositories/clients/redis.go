package redis

import (
	"fmt"
	"os"
	"strings"

	"github.com/go-redis/redis"
)

type redisClient struct {
	client *redis.Client
}

func New() redisClient {
	var REDIS_HOST = os.Getenv("REDIS_HOST")
	if REDIS_HOST == "" {
		REDIS_HOST = "localhost"
	}
	var REDIS_PORT = os.Getenv("REDIS_PORT")
	if REDIS_PORT == "" {
		REDIS_PORT = "6379"
	}

	c := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", REDIS_HOST, REDIS_PORT),
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	_, err := c.Ping().Result()
	if err != nil {
		panic(fmt.Sprintf("Could not connect to redis on %s:%s", REDIS_HOST, REDIS_PORT))
	}

	return redisClient{
		c,
	}
}

func (rc redisClient) Get(name string) (string, error) {
	return rc.client.Get("nuclei.service-" + name).Result()
}

func (rc redisClient) Set(name string, value string) error {
	return rc.client.Set("nuclei.service-"+name, value, 0).Err()
}

func (rc redisClient) GetAllKeys() ([]string, error) {
	keys, err := rc.client.Keys("nuclei.service-*").Result()

	if err != nil {
		return make([]string, 0), nil
	}

	for i, key := range keys {
		keys[i] = strings.Replace(key, "nuclei.service-", "", 1)
	}

	return keys, nil
}
