package repositories

import (
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"fmt"

	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/dto"
)

type ServiceRepo struct {
	Client KeyValueClient
}

type KeyValueClient interface {
	GetAllKeys() ([]string, error)
	Get(string) (string, error)
	Set(string, string) error
}

func New(client KeyValueClient) ServiceRepo {
	return ServiceRepo{
		client,
	}
}

func (sr ServiceRepo) GetService(name string) (dto.Service, error) {
	s, err := sr.Client.Get(name)
	if err != nil {
		return dto.Service{}, err
	}

	return FromGOB64(s), nil
}

func (sr ServiceRepo) StoreService(service dto.Service) error {
	return sr.Client.Set(service.Name, ToGOB64(service))
}

func (sr ServiceRepo) GetAllServices() ([]dto.Service, error) {
	keys, err := sr.Client.GetAllKeys()

	if err != nil {
		return make([]dto.Service, 0), nil
	}

	services := make([]dto.Service, len(keys))

	for i, key := range keys {
		services[i], err = sr.GetService(key)
	}

	return services, nil
}

func ToGOB64(m dto.Service) string {
	b := bytes.Buffer{}
	e := gob.NewEncoder(&b)
	err := e.Encode(m)
	if err != nil {
		fmt.Println(`failed gob Encode`, err)
	}
	return base64.StdEncoding.EncodeToString(b.Bytes())
}

func FromGOB64(str string) dto.Service {
	m := dto.Service{}
	by, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		fmt.Println(`failed base64 Decode`, err)
	}
	b := bytes.Buffer{}
	b.Write(by)
	d := gob.NewDecoder(&b)
	err = d.Decode(&m)
	if err != nil {
		fmt.Println(`failed gob Decode`, err)
	}
	return m
}
