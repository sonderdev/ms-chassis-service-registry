package relay

import (
	"testing"

	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/dto"
	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/validation/service"
)

func TestNewServiceRequestValidation(t *testing.T) {

	// TEST 1

	s := dto.NewServiceRequest{
		"avalidname",
		"http://avalidlocation",
		"/avaliduri",
		[]string{},
		[]string{},
	}

	valid := serviceValidation.ValdidateNewServiceRequest(s)

	if !valid {
		t.Errorf("This should have been valid")
	}

	// TEST 2

	s = dto.NewServiceRequest{
		"avalidname",
		"aninvalidlocation",
		"/avaliduri",
		[]string{},
		[]string{},
	}

	valid = serviceValidation.ValdidateNewServiceRequest(s)

	if valid {
		t.Errorf("This should have been invalid")
	}

	// TEST 3

	s = dto.NewServiceRequest{
		"avalidname",
		"http://avalidlocation",
		"aninvaliduri",
		[]string{},
		[]string{},
	}

	valid = serviceValidation.ValdidateNewServiceRequest(s)

	if valid {
		t.Errorf("This should have been invalid")
	}

	// TEST 4

	s = dto.NewServiceRequest{
		"",
		"http://avalidlocation",
		"/avaliduri",
		[]string{},
		[]string{},
	}

	valid = serviceValidation.ValdidateNewServiceRequest(s)

	if valid {
		t.Errorf("This should have been invalid")
	}

	// TEST 5

	s = dto.NewServiceRequest{
		"abcdefghijklmnopqrstuvwxyz1234567", // 33 characters (max 32)
		"http://avalidlocation",
		"/avaliduri",
		[]string{},
		[]string{},
	}

	valid = serviceValidation.ValdidateNewServiceRequest(s)

	if valid {
		t.Errorf("This should have been invalid")
	}
}
