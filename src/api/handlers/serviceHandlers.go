package serviceHandlers

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/dto"
	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/logging"
	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/validation/service"
)

type ServiceStorer interface {
	StoreService(dto.Service) error
	GetAllServices() ([]dto.Service, error)
}

type ServiceHandler struct {
	client ServiceStorer
	logger *logging.Logger
}

func New(client ServiceStorer, logger *logging.Logger) ServiceHandler {
	return ServiceHandler{
		client,
		logger,
	}
}

func (sh ServiceHandler) ServiceRegistrationHandler(w http.ResponseWriter, r *http.Request) {
	var nsr dto.NewServiceRequest

	// get the object from body and deserialize it
	_ = json.NewDecoder(r.Body).Decode(&nsr)

	// validate it
	valid := serviceValidation.ValdidateNewServiceRequest(nsr)
	if !valid {
		w.WriteHeader(http.StatusBadRequest)
		io.Copy(w, strings.NewReader("Service is not valid"))
		return
	}

	// convert it
	s := dto.Service{
		nsr.Name,
		nsr.Location,
		nsr.QueueEvents,
		nsr.TopicEvents,
		nsr.HealthEndpoint,
		"alive",
		"closed",
		3,
		time.Now(),
	}

	// store it
	sh.client.StoreService(s)

	// log it
	sh.logger.LoggingClient.LogService(&s)

	// return a response
	w.WriteHeader(http.StatusCreated)
	io.Copy(w, strings.NewReader("Service registered"))
}

func (sh ServiceHandler) ListServicesHandler(w http.ResponseWriter, r *http.Request) {
	// get all services from Redis
	services, err := sh.client.GetAllServices()

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		io.Copy(w, strings.NewReader("Could not get services"))
		return
	}

	// serialize and return response
	json.NewEncoder(w).Encode(services)
}
