package logging

import "bitbucket.org/sonderdev/ms-chassis-service-registry/src/dto"

type LoggingClient interface {
	LogService(*dto.Service)
}

type Logger struct {
	LoggingClient LoggingClient
}

func NewLogger(lc LoggingClient) *Logger {
	return &Logger{
		lc,
	}
}
