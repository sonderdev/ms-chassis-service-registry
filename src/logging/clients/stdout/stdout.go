package stdout

import (
	"fmt"

	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/dto"
)

type stdOutLogger struct {
}

func New() *stdOutLogger {
	sol := stdOutLogger{}
	return &sol
}

func (std stdOutLogger) LogService(l *dto.Service) {
	fmt.Printf("%+v\n", *l)
}
