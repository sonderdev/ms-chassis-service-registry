package elasticsearch

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/dto"
	uuid "github.com/satori/go.uuid"

	elastic "gopkg.in/olivere/elastic.v5"
)

type elasticsearchLogger struct {
	client *elastic.Client
}

func New() *elasticsearchLogger {
	elasticURL := os.Getenv("LOGGING_ELASTIC_URL")
	client, err := elastic.NewSimpleClient(elastic.SetURL(elasticURL))
	if err != nil {
		panic(err)
	}

	esl := elasticsearchLogger{}
	esl.client = client

	return &esl
}

func (esl *elasticsearchLogger) LogService(l *dto.Service) {
	u, _ := uuid.NewV4()
	index := "service-registry-" + time.Now().Format("2006-01-02")
	ctx := context.Background()
	_, err := esl.client.Index().Index(index).Type("service").Id(fmt.Sprint(u)).BodyJson(&l).Do(ctx)
	log.Println(err)
}
