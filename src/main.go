package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/logging"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"

	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/api/handlers"
	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/factories"
	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/repositories"
	"bitbucket.org/sonderdev/ms-chassis-service-registry/src/repositories/clients"
)

func main() {
	// load env variables
	err := godotenv.Load()
	if err != nil {
		log.Print("Error loading .env file")
	}

	loggingClient := factories.NewLoggingClient()
	logger := logging.NewLogger(loggingClient)

	client := redis.New()
	repo := repositories.New(client)

	nsh := serviceHandlers.New(&repo, logger)

	router := mux.NewRouter()

	router.HandleFunc("/", nsh.ListServicesHandler).Methods("GET")
	router.HandleFunc("/", nsh.ServiceRegistrationHandler).Methods("POST")

	// get the port
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	srv := &http.Server{
		Handler:      router,
		Addr:         os.Getenv("HOST") + ":" + port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
