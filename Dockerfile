FROM golang:1.10.3 AS build-env
WORKDIR /go/src/bitbucket.org/sonderdev/ms-chassis-service-registry/src/
ADD src . 
RUN go get github.com/gorilla/mux
RUN go get github.com/go-redis/redis
RUN go get gopkg.in/go-playground/validator.v9
RUN go get github.com/joho/godotenv
RUN go get gopkg.in/olivere/elastic.v5
RUN go get github.com/satori/go.uuid
RUN go test -v ./tests	
RUN CGO_ENABLED=0 GOOS=linux go build -o main.exe main.go

FROM scratch
COPY --from=build-env /go/src/bitbucket.org/sonderdev/ms-chassis-service-registry/src/main.exe .
COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ENV PORT=80
ENV HOST=
ENV REDIS_HOST=redis
ENTRYPOINT  ["./main.exe"]
EXPOSE 80